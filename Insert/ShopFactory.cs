﻿using Autofac;
using Insert.DTOs;
using Insert.Interface;
using Insert.MongoDb;

namespace Insert
{
    public class ShopFactory : IShopFactory
    {
        private readonly MongoDbHelper _mongoDbHelper;

        public ShopFactory(MongoDbHelper mongoDbHelper)
        {
            _mongoDbHelper = mongoDbHelper;
        }

        public IShopData InsertShopData(int id, string name, int price)
        {
            var toInsert = new ShopDTO { Id = id, Name = name, Price = price };
            _mongoDbHelper.Insert(toInsert);

            return toInsert as IShopData;
        }
    }
}
