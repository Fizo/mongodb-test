﻿using System.Threading.Tasks;
using Autofac;
using Insert.DTOs;
using Insert.Interface;
using Insert.MongoDb;

namespace Insert
{
    public class Program
    {
        private static IContainer Container { get; set; }

        static async Task Main(string[] args)
        {
            var builder = new ContainerBuilder();
            builder.Register(s => new MongoDbHelper()).As<MongoDbHelper>();
            builder.Register(s => new ShopFactory(new MongoDbHelper())).As<IShopFactory>();
            Container = builder.Build();

            var insert = Container.Resolve<IShopFactory>();
            insert.InsertShopData(1, "test", 200);
        }
    }
}
