﻿using System;
using System.Threading.Tasks;
using Insert.DTOs;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Insert.MongoDb
{
    public class MongoDbHelper
    {
        private readonly MongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;

        public MongoDbHelper()
        {
            _mongoClient = new MongoClient("mongodb://root:root@localhost:27017");
            _mongoDatabase = _mongoClient.GetDatabase("test_insert");
        }

        public bool CheckCollection<T>()
        {
            var filter = new BsonDocument("name", typeof(T).Name);
            var collectionCursor = _mongoDatabase.ListCollections(new ListCollectionsOptions { Filter = filter });
            return collectionCursor.Any();
        }

        public bool Insert<T>(T document) where T : IDto
        {
            try
            {
                if (!(CheckCollection<T>()))
                {
                    _mongoDatabase.CreateCollection(typeof(T).Name);
                    _mongoDatabase.GetCollection<T>(typeof(T).Name).InsertOne(document);
                    return true;
                }

                _mongoDatabase.GetCollection<T>(typeof(T).Name).FindOneAndReplace(s => s.Id == document.Id, document);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}
