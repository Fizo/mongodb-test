﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Insert.DTOs
{
    public class ShopDTO : IDto
    {
        [BsonId]
        public int Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("price")]
        public int Price { get; set; }
    }
}
