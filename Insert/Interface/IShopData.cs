﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insert.Interface
{
    public interface IShopData
    {
        int Id { get; set; }

        string Name { get; set; }

        int Price { get; set; }
    }
}
