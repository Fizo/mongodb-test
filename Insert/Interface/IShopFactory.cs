﻿namespace Insert.Interface
{
    public interface IShopFactory
    {
        IShopData InsertShopData(int id, string name, int price);
    }
}
