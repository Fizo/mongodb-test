﻿using System.Threading.Tasks;
using Insert.DTOs;
using Insert.UnitTest.MongoDb;
using NFluent;
using Xunit;

namespace Insert.UnitTest
{
    public class InsertTest
    {
        [Fact]
        public void InsertIfTheCollectionAlreadyExists()
        {
            MongoDbHelperMock mongoDbHelper = new MongoDbHelperMock();

            Check.That(mongoDbHelper.asyncInsertIfCollectionExists(new ShopDTO { Id = 1, Name = "Test", Price = 2000})).Equals(true);
        }

        [Fact]
        public void InsertIfTheCollectionNotExist()
        {
            MongoDbHelperMock mongoDbHelper = new MongoDbHelperMock();

            Check.That(mongoDbHelper.asyncInsert(new ShopDTO { Id = 1, Name = "Test", Price = 2000 })).Equals(true);
        }

    }
}
