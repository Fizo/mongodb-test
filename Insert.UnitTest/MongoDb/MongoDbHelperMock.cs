﻿using System;
using System.Threading.Tasks;
using Insert.DTOs;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Insert.UnitTest.MongoDb
{
    public class MongoDbHelperMock
    {
        private readonly MongoClient _mongoClient;
        private readonly IMongoDatabase _mongoDatabase;

        public MongoDbHelperMock()
        {
            _mongoClient = new MongoClient("mongodb://root:root@localhost:27017");
            _mongoDatabase = _mongoClient.GetDatabase("mock_insert");
        }

        public bool CheckCollection<T>()
        {
            var filter = new BsonDocument("name", typeof(T).Name);
            var collectionCursor = _mongoDatabase.ListCollections(new ListCollectionsOptions { Filter = filter });
            return collectionCursor.Any();
        }

        public bool asyncInsertIfCollectionExists<T>(T document) where T : IDto
        {
            try
            {
                if (!CheckCollection<T>())
                {
                    _mongoDatabase.CreateCollection(typeof(T).Name);
                    _mongoDatabase.GetCollection<T>(typeof(T).Name).InsertOneAsync(document); // Fake insertion
                }

                _mongoDatabase.GetCollection<T>(typeof(T).Name).FindOneAndReplaceAsync(s => s.Id == document.Id, document);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            _mongoDatabase.DropCollectionAsync(typeof(T).Name);
            return true;
        }

        public bool asyncInsert<T>(T document)
        {
            try
            {
                if (CheckCollection<T>())
                {
                    _mongoDatabase.DropCollectionAsync(typeof(T).Name); // delete collection if exist
                } 

                _mongoDatabase.CreateCollection(typeof(T).Name);
                _mongoDatabase.GetCollection<T>(typeof(T).Name).InsertOneAsync(document);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;
        }
    }
}